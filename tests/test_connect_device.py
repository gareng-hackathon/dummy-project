import napalm


def test_get_device_interface():
    # changing code
    device_data = {
        'driver_name': 'iosxr',
        'hostname': "sandbox-iosxr-1.cisco.com",
        'username': "admin",
        'password': "C1sco12345",
        'optional_args': {'port': 22},
    }

    # Napalm sandbox
    driver = napalm.get_network_driver(device_data.pop('driver_name'))
    device = driver(**device_data)
    assert device

    device.open()
    facts = device.get_facts()
    assert facts
